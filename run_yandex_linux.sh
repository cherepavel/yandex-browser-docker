docker run --rm \
           -e DISPLAY=:0 \
           -v /tmp/.X11-unix:/tmp/.X11-unix \
           -v ~/.Xauthority:/root/.Xauthority \
           -v $PWD/yandex_browser/config:/root/.config/yandex-browser \
           -v $PWD/yandex_browser/data:/root/.yandex \
           --name yandex-browser yandex-browser:latest
