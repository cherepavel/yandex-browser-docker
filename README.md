# yandex-browser-docker


### Сборка 
* Скачайте deb пакет`yandex-browser-stable_24.4.1.951-1_amd64.deb` (ссылка: https://repo.yandex.ru/yandex-browser/deb/pool/main/y/yandex-browser-stable/yandex-browser-stable_24.4.1.951-1_amd64.deb)
* Положите его папку src
* Выполните `build.sh` (если Windows, то `build.bat`)
* В зависимости от системы выполните скрипт для запуска (windows - `run_yandex_win.bat`, mac - `run_yandex_mac.sh`, linux - `run_yandex_linux.sh`)

---
![alt text](screen.jpg)