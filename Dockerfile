FROM --platform=linux/amd64 ubuntu:22.04
LABEL authors="Pavel Frasyn"

COPY src/yandex-browser-stable_24.4.1.951-1_amd64.deb /tmp

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y /tmp/yandex-browser-stable_24.4.1.951-1_amd64.deb \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \

CMD ["yandex-browser", "--no-sandbox", "--disable-gpu", "--disable-features=dbus", "--homepage=about:blank", "--no-first-run", "--disable-dev-shm-usage", "--disable-accelerated-2d-canvas", "--disable-canvas-aa", "--disable-2d-canvas-clip-aa", "--disable-gl-drawing-for-tests", "--disable-extensions", "--disable-sync", "--metrics-recording-only", "--disable-default-apps", "--no-crash-upload", "--ignore-certificate-errors", "--use-gl=swiftshader", "--disable-software-rasterizer", "--disable-breakpad", "--disable-crashpad", "--font-render-hinting=none", "--no-default-browser-check"]
